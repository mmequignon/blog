Connecting to postgresql with dbeaver using unix sockets
========================================================

Socat
-----

We can connect to PostgreSQL using Unix sockets.
It means that instead of specifying credentials and be prompted for your password::

    psql -h localhost -U mmequignon -p 5432 -d mydatabase

We can do this, as long as ``$WHOAMI`` is also a PostgreSQL user::

    psql mydatabase

And you will not be prompted for you password, which is very convenient for local developments.

However, the PostgreSQL DBeaver driver doesn't work with unix sockets,
meaning that you have to write down everything, which is very annoying.

But all hope is not lost, there's a tool for this: ``socat``

Socat is a program acting as a proxy, forwarding data from (almost) any source, to (almost) any destination. 
It means that we can send data through TCP (as DBeaver does), and ask socat to forward this data to an Unix socket.

The command I'm using is::

  socat TCP4-LISTEN:15432,fork,reuseaddr UNIX-CONNECT:/run/postgresql/.s.PGSQL.5432

here's the `manpage <https://linux.die.net/man/1/socat>`_

  
- ``TCP4-LISTEN:15432`` will listen for TCP/IP connections on the port 15432

  - ``fork``: Create child processes for each connection
  - ``reuseaddr``: allows immediate restart of the server process
- ``UNIX-CONNECT:/run/postgresql/.s.PGSQL.5432`` is the destination, and it expects a socket as argument.
  Here I'm targeting the postgresql socket.

.. note::
    I'm using nixos, the path to the socket might vary (it was in /var/run/postgresql on Ubuntu).

Now I can run the first command and no password is asked to me anymore. Nice::
  
  psql -h localhost -p 15432 -d mydatabase

And I can now connect onto it with DBeaver.

--------------------------------------------

Systemd service with Nix and home-manager
-----------------------------------------

nothing much to say here.
Here's the file.::

    { config, pkgs, ...}:

    let
      PG_PATH = "/run/postgresql";
      PG_SOCKET = "${PG_PATH}/.s.PGSQL.5432";
      SOCAT = "${pkgs.socat}/bin/socat";
      SOCAT_TEMPLATE = FROM: TO: {
        Unit = {
          Description = "socat ${FROM} -> ${TO}";
        };
        Service = {
          ExecStart = "${SOCAT} -d -d -d -d ${FROM},fork,reuseaddr ${TO}";
          Type = "exec";
        };
        Install = {
          WantedBy = ["multi-user.target"];
        };
      };
    in
    {
      systemd.user.services = {
        socat_postgresql = SOCAT_TEMPLATE "TCP4-LISTEN:15432" "UNIX-CONNECT:${PG_SOCKET}";
      };
    }
