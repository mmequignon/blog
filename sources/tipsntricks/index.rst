Tips 'n Tricks
==============

.. toctree::
   :maxdepth: 1 
   :caption: Tips 'n Tricks

   socat
   x2x
   vim
   deb_packages
   acpi-systemd
   virtualenvs
   migration-mails
