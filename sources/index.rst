.. Fwzte documentation master file, created by
   sphinx-quickstart on Sat Mar  4 20:03:41 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

$ whoami
========

About me
++++++++

Hi, my name is `Matthieu <https://github.com/mmequignon>`_.

I hold a master's degree in software development with a strong focus on FLOSS.

I'm a Python enthusiast, a fervent defender of Free Software and a proud member of the `April <https://www.april.org/>`_.

I also like to play the guitar.

Contact
+++++++

 - E-mail : ``matthieu [at] fwzte [dot] xyz``
 - IRC : ``MmeQuignon`` on `freenode <https://freenode.net/>`_

 :download:`Here is my CV <_static/matthieu_mequignon_cv.pdf>`


Categories
++++++++++
.. toctree::

   Blog <blog/index>
   Tips 'n Tricks <tipsntricks/index>
   Scripts <scripts/index>
   Music <music/index>
