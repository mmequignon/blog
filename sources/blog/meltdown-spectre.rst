Meltdown & Spectre
==================

.. figure::  ../_static/meltdown_and_spectre.png
   :align: center
   :width: 30%


What is an operating system?
----------------------------
It's a software whose role is to act as an interface between hardware and sofware layers.
It deals with system calls coming from processes thanks to the scheduler.

.. figure:: ../_static/latencies.png
   :align: center
   :width: 80%

If the CPU was executing all tasks as sent by the OS, it would just idle most of the time.

That's why some optimisations are made CPU side.


How the CPU works?
------------------

CPU cache
+++++++++

After the CPU asks the main memory for data, it can execute around 200 instructions before it gets it.

However, processes tends to reach the main memory in a predictable way:
 - If a process accesses a specific area, it is likely to access it again;
 - If a process accesses a specific area, it is likely to access nearby area soon.


Two CPU memory caches with quick access times (0,5ns and 7ns), named L1 and L2, are meant to store this data.w

.. note::
   We can easily deduct the origin of a data:
    - Access time < 7ns → from the cache;
    - Access time > 7ns → from the main memory.

Out of order execution
++++++++++++++++++++++

A superscalar processor gets instructions stacks and tries to parrallelize executions. In the following cases, the CPU will be able to execute two instructions simultaneously:
We say it have two free **execution slots** per cycle.

.. code-block:: python

   t, u = a+b, c+d
   v, w = e+f, v+g
   x, y = h+i, j+k

Here, <<w>> is based on the result of <<v>> which is computed in the same instruction.

.. code-block:: python

   t, u = a+b, c+d
   v = e+f
   w, x = v+g, h+i
   y = j+k

What is actually executed.

.. code-block:: python

   t, u = a+b, c+d
   v, x = e+f, h+i
   w, y = v+g, j+k

A CPU that can make out of order executions is able to reoder incoming instructions.

Speculation & branch prediction
+++++++++++++++++++++++++++++++

**Spectulation** consists in precalculating instructions before knowing the result of their predicates.
The result is then placed in the process memory space waiting for the predicate evaluation.

.. code-block:: python

   if open("toto.txt", "w"):
       a = 27 + 15

The purpose of **branch prediction** is to guess the result of predicates in order to reduce instructions execution.
The main goal here is to try to fill **execution slots** more efficiently.

.. figure:: ../_static/speculative_execution.png
   :align: center
   :width: 30%

.. note::
   - There's a time window where mis-speculation results stay in cache ~5ns.
   - The branch predictor can be trained to make wrong speculations in order to make data available in the cache nevertheless.

Priviledged & user modes
++++++++++++++++++++++++

 All processes are running in **user mode** except the operating system, running in **kernel mode** (or priviledged mode).

.. figure:: ../_static/user_kernel_modes.png
   :align: center
   :width: 40%

Each process have its own memory space and process isolation is supposed to be guaranteed.
When a user process asks access to the memory space of another one, it just gets an exception.

Only processes running in kernel mode are able to see data in other memory spaces.

.. note::
   | When a process asks for access to a memory space, a verification is made.
   | It is possible to prepend the memory request by a predicate in order to differ the resulting exception.
   | Then, the data is placed in cache during the **mis-speculation window** even if access is denied later.

Spectre & Meltdown
------------------

Putting it all together
+++++++++++++++++++++++

Let's consider this program that sometimes tries to read in the kernel space:

.. code-block:: python

   t = a+b
   u = t+c
   v = u+d
   if v:
      w = kern_mem[address]   # if we get here, fault
      x = w & 0x80            # 0x80 == 255 == Ob10000000
      y = user_mem[x]

We now consider that the branch predictor is trained to believe that **"v"** is likely to be non-zero in order to speculate on **"w"**, **"x"** and **"y"**.
Then, our out-of-order superscalar processor orders instructions like this:

.. code-block:: python

   t, w_ = a+b, kern_mem[address]
   u, x_ = t+c, w_ & 0x80
   v, y_ = u+d, user_mem[x_]
   if v: # fault
      w, x, y = w_, x_, y_      # we never get here

The goal here is to read the 8th bit from **"w_"**.
We will consider that we have actually flushed our cache so each memory fetch should take around 100ns.
Now let's focus on those lines:

.. code-block:: python

   t, w_ = a+b, kern_mem[address] # w_ contains a Byte from kernel space
   u, x_ = t+c, w_ & 0x80         # x_ equals 128 or 0 depending on the 8th bit of w_
   v, y_ = u+d, user_mem[x_]

We know for sure that **"x_"** is either **128** or **0**.
Both results means that the 8th bit from the kernel space data is either **1** or **0**.
So, if any of **128** or **0** userspace addresses takes around 7ns to fetch, we can deduce the value of this bit.

That's it, you've read a bit from the priviledged adress space !

References
----------

 - `Latency Numbers Every Programmer Should Know <https://gist.github.com/jboner/2841832>`_
 - `Why Raspberry Pi isn’t vulnerable to Spectre or Meltdown <https://www.raspberrypi.org/blog/why-raspberry-pi-isnt-vulnerable-to-spectre-or-meltdown/>`_
