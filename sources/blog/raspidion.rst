*********
Raspidion
*********

A few days ago, a collegue of mine shared this youtube video on slack, which I totally fell in love with.

.. raw:: html

       <iframe width="560" height="315" src="https://www.youtube.com/embed/EBCYvoC4muc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Since I watched this, I want to build the same instrument.
I basically know nothing about commodore64.
No big deal, I will do it with a raspberry pi pico.

The idea
++++++++

 * The goal is to be able to play sound when I press keys.
 * Ideally, I should be able to select the instrument (or sound) directly from the device.
 * I'd like to either send MIDI data via usb or play a sound (configurable).
 * A few modal keys (lockable or not) to add trills, vibrato, add or remove an octave from a note etc.
 * Probably a lot more to add here.

| There's nothing I know about what I'm about to do.
| My knowledge about microcontrollers is near inexistent, So I've downloaded this `book <https://store.rpipress.cc/collections/latest-bookazines/products/get-started-with-micropython-on-raspberry-pi-pico>`_.
| I also bought the `raspberry pi pico <https://www.raspberrypi.com/products/raspberry-pi-pico>`_ as well as a starter pack which includes a breadboard, a few wires, buttons and so on.
| 
| I know nothing about sound theory and electronic music either, but it's all right, I'll learn.

The project
+++++++++++

2022-11-06
----------

| Last friday (the 4th of November), I received my raspi as well as the starter kit.
| With the help of the aforementioned book, I was able to quickly light up leds, control them with buttons.
| I'm quite impressed about how simple it is to deal with external devices.
| It is as simple as selecting a pin, configuring it (is it an input pin or an ouput one), and getting or setting value from/to it.

.. code-block:: python

    # With micropython
    from machine import Pin

    led = Pin(<PIN NUMBER>, Pin.OUT)
    button = Pin(<PIN NUMBER>, Pin.IN, pull=Pin.PULL_DOWN)

    current_state = button.value()
    while True:
        new_state = button.value()
        if current_state != new_state:
            led.value(new_state)
            current_state = new_state

| I was quickly able to control four leds (because I only had 4 buttons Y_Y).
| Then, I quickly moved from `micropython <https://www.micropython.org/>`_ to `circuitpython <https://circuitpython.org/>`_.
| The main reason for that swithch is that microptyhon provided the `midi_usb <https://docs.circuitpython.org/en/latest/shared-bindings/usb_midi/index.html#>`_ library.
| Also, I was able to use the `adafruit_midi <https://docs.circuitpython.org/projects/midi/en/latest/api.html>`_ library.
| I traded the possibility to do polyphony against the possibility to play up to 15 notes.
| Now, each one of the button corresponds to a bit.
| Each time a button is pressed or released, the state change.
| If a note was played before, it is stopped, and a new one (if any button is pressed) is sent through usb.
| This was straightforward as well, and a few minutes later, I was able to play notes on my computer with
| the raspberry pico as a MIDI controller, which is quite awesome.
| Here's the code, as of today.

.. code-block:: python

    # With circuitpython
    import digitalio
    import board
    import time
    import usb_midi
    import adafruit_midi
    from adafruit_midi.note_on import NoteOn
    from adafruit_midi.note_off import NoteOff
    midi = adafruit_midi.MIDI(midi_out=usb_midi.ports[1], out_channel=0)

    # Each button has its own led.
    pin_mapping = [
        ("GP2", "GP3"),
        ("GP6", "GP7"),
        ("GP10", "GP11"),
        ("GP14", "GP15"),
    ]

    class Led:
        def __init__(self, name):
            pin = getattr(board, name)
            self.name = name
            self.led = digitalio.DigitalInOut(pin)
            self.led.direction = digitalio.Direction.OUTPUT

        def toggle(self):
            self.led.value = not self.led.value

        def set(self, value):
            self.led.value = value


    class Button:
        def __init__(self, button_pin_name, led_pin_name):
            pin = getattr(board, button_pin_name)
            self.name = button_pin_name
            self.button = digitalio.DigitalInOut(pin)
            self.button.direction = digitalio.Direction.INPUT
            self.button.pull = digitalio.Pull.DOWN
            self.led = Led(led_pin_name)
            self.value = False

        def get_current_value(self):
            return self.button.value

        def update_value(self, value):
            self.value = bool(value)
            self.led.set(self.value)

    class ButtonSet:
        def __init__(self, mapping):
            self.buttons = []
            for button_pin, led_pin in mapping:
                self.buttons.append(Button(button_pin, led_pin))
            value = self.get_current_value()
            self.update_value(value)

        def has_changed(self, value):
            return self.value != value

        def get_current_value(self):
            return [int(b.get_current_value()) for b in self.buttons]

        def update_value(self, value):
            self.value = value
            for button, value in zip(self.buttons, value):
                button.update_value(value)

        def get_note(self):
            res = 0
            if sum(self.value):
                res += 60
                bin_string = "".join(str(v) for v in self.value)
                res += int(bin_string, 2)
            return res

        def get_freq(self):
            midi_note = self.get_note()
            if midi_note:
                return round(440*(2**((midi_note-69)/12)), 2)


    button_set = ButtonSet(pin_mapping)

    while True:
        value = button_set.get_current_value()
        if button_set.has_changed(value):
            midi_message = []
            # turnoff current note played, if any.
            note = button_set.get_note()
            if note:
                midi_message.append(NoteOff(note))
            button_set.update_value(value)
            new_note = button_set.get_note()
            if new_note:
                new_freq = button_set.get_freq()
                midi_message.append(NoteOn(new_note))
            if midi_message:
                midi.send(midi_message)
                print(f"value {midi_message} sent")

| And that's it. With this code, when I press on keys, I send MIDI signal to the computer it is connected to.
| It also lightens up the corresponding leds (this helpful to debug).
| The code is not really clean, but I'm ok with that: This code isn't meant to be used (yet).
| There's no schema for the board, but this is coming.

| Notes: MIDI is quite simple. Each message consists of 3 bytes:

 * byte 1:

   * the four first bits are the status (on / off)
   * the four last bits are the channel

 * byte 2 contains the note
 * byte 3 contains the velocity

| Therefore, since it seems easy to make, and in order to lighten the program, I might drop «adafruit_midi» in the future.

2022-11-08
----------

| I received a `74hc595 shift register <https://thepihut.com/products/74hc595-shift-register-3-pack?variant=27740425553&currency=GBP&srsltid=AYJSbAdLOn0hFoXHM3S9RVPCYVKTkvxawBb0BhsiecfIdFx_LlkBQpBEW0I>`_ with my starter pack.
| As I understand it, this chip is supposed to provide 8 extra pins at the cost of 5 pins.
| In those 5 pins, two are power output and ground pins, which doesn't really count as «used pins», since we aren't limited to one connection on those.

.. figure:: ../_static/74HC595_pinout.png
   :align: center
   :width: 30%

   (source: `adafruit.com <https://learn.adafruit.com/74hc595/pinouts>`_)

| On this schema, we can find 8 pins named QA, QB, …, QH (from 0 to 7).
| Those are the pins we will be able to use afterwards.

.. figure:: ../_static/74HC959_internal_diagram.png
   :align: center
   :width: 30%

   (source: `filipeflop.com <https://www.filipeflop.com/blog/como-usar-o-ci-74hc595-shift-register>`_)

| Regarding this last diagram, we see that there's 5 inputs.

 - 1-7 and 15 → Are the extra pins you get
 - 8 → ground
 - 9 → Don't know  what this is…
 - 10 → SRCLR → Reset
 - 11 → SRCLK → Shift clock
 - 12 → RCLK → Latch clock
 - 13 → OE → Output Enable
 - 14 → SER → Serial data input
 - 16 → Vcc - Power in

| Here's the `documentation <https://docs.circuitpython.org/projects/74hc595/en/latest/index.html>`_ of the adafruit's 74HC595 library.
| On a lot of documentation (including) on the Internet, you'll find pin names that those of some other microcontrollers.
| Thankfully, there's this `page <https://learn.adafruit.com/getting-started-with-raspberry-pi-pico-circuitpython/pinouts>`_, where we can find the correspondances.
| What's interesting here is the SPI example.

 - SPIx_SCK = SCK
 - SPIx_TX = MOSI
 - SPIx_RX = MISO

| For my tests, all I needed was the SCK and the MOSI ports.

.. figure:: ../_static/raspico_pinout.png
   :align: center
   :width: 40%

   (source: `raspberrypi.com <https://datasheets.raspberrypi.com/pico/Pico-R3-A4-Pinout.pdf>`_)

| Here's the wiring I came up with.

.. figure:: ../_static/74HC595_pico_wiring.png
   :align: center
   :width: 40%

| And the code I came up with.

.. code-block:: python

    import digitalio
    import adafruit_74hc595
    import busio
    import board

    # This is the `data` pin. I suppose we could use any pin for that.
    latch = digitalio.DigitalInOut(board.GP13)
    # As I understand it, we need to use GPIn_SCK as the clock pin,
    # and SPIn_TX as the mosi pin, as long the `n` matches.
    # This means that if n == 1, the clock pin could be GP14 or GP10
    # and the mosi pin could be GP15 or GP11.
    clock_pin = board.GP14
    mosi_pin = board.GP15
    # Now instanciate the spi bus, and the shift_register
    spi = busio.SPI(clock=clock_pin, MOSI=mosi_pin)  
    shift_register = adafruit_74hc595.ShiftRegister74HC595(spi, latch)

    class Led:
        def __init__(self, name):
            self.name = name
            # Getting a pin is as easy as that.
            self.led = shift_register.get_pin(name)

        def toggle(self):
            self.led.value = not self.led.value

    # Instanciating a led, attached to the pin 1
    # Pin names are 0, …, 7
    # Be careful, pin 15 as seen in the diagrams isn't QA.
    # QA -> pin 0
    led = Led(0)
    while True:
        # Yeah, it blinks.
        led.toggle()
        time.sleep(0.5)
        led.toggle()
        time.sleep(0.5)

You probably noticed that I do not set the direction on the pin.
This is because the 74HC595 extra pins are output only pins only.
This means that I cannot set a pin to Direction.OUTPUT, and cannot attach buttons.
I'll have to try with another chip, which allows both input and input.

2023-02-11
----------

It was a long time since I posted something about this project…
Much things to do, not that much time to do them.

I received my stuff, and managed a basic matrix keyboard.

.. figure:: ../_static/raspidion/matrix_keyboard_front.jpg
   :align: center
   :width: 30%

.. figure:: ../_static/raspidion/matrix_keyboard_back.jpg
   :align: center
   :width: 30%

   (A diode here is inverted. This is fixed)

I received my GPIO expanders, and am currently struggling to make them work.
The multimeter is ok with my keyboard, but I do not get the expected result from the expander.
Not a big deal, I'll deal with that later.

What I'm trying to do as of now, is to play multiple sounds from a speaker attached to the raspberry pi.

I've tried a few things, first to use the audio mixer module, which results in a aweful sound
Now, I'm trying to sum signals generated from frequencies.
The idea is that given a frequency, we get a sine wave.

.. figure:: ../_static/raspidion/sine_wave_440.svg
   :align: center
   :width: 30%

   A 440Hz period wave

Not let's say I want to play an A chord.
The frequencies are 440Hz, 523.25Hz and 659.26Hz.
What we have to know here is that, what we hear is actually a sum of distinct waves.
As Fourier demonstrated, a complex wave can be decomposed as multiple simple waves (like the 440Hz one above).
And the opposite should be true.
I should be able to sum multiple waves in order to create a complex one.
So, for the A chord, those are the 3 waves.

.. figure:: ../_static/raspidion/sine_wave_440_523_659.svg
   :align: center
   :width: 30%

   440Hz, 523.25Hz 659.25Hz waves

What I get by summing those waves is something like this.

.. figure:: ../_static/raspidion/sine_wave_sum.svg
   :align: center
   :width: 30%

   440Hz, 523.25Hz 659.25Hz with the sum wave

Here's the code to play this sound

.. code-block:: python

    import audiocore
    import audiomixer
    import audiopwmio
    import board
    import array
    import time
    import math

    SAMPLE_RATE = 10_000
    WAVE_HEIGHT = (2**15 // 3)

    class Note:
        sample_rate = SAMPLE_RATE
        def __init__(self, frequency):
            self.frequency = frequency
            self.length = math.floor(self.sample_rate // self.frequency)
            self.sine_wave = self.get_sine_wave()

        def get_sine_wave(self):
            sine_wave = []
            for index in range(self.length):
                portion = math.sin(math.pi * 2 * index / self.length)
                two_byte_val = portion * WAVE_HEIGHT
                sine_wave.append(math.floor(two_byte_val))
            return sine_wave

    class Chord:
        sample_rate = SAMPLE_RATE

        def __init__(self, notes):
            self.notes = notes
            # Note.get_sine_wave() picks a sample every 1/8000th of a second.
            # Depending on the frequency we will have a different amount of samples.
            # I.E.
            #  - 440 Hz -> 8000/440 = 18 samples
            #  - 880 Hz -> 8000/880 = 9 samples
            # If we want to loop over those samples and we play multiple notes at
            # once, we do not want to "break" a wave and start from the first sample
            # while the previous one wasn't finished.
            # To do so, we have to find the sample number where all waves are converging.
            # This number is convergence is the product of the number of samples for each
            # note.
            # This is the smallest convegence sample number
            self.scsn = 1
            for note in self.notes:
                self.scsn *= len(note.sine_wave)
            self.waves = self._get_note_waves()
            self.wave = self._get_sine_wave()
            self.sample = self._get_sample()

        def _get_sample(self):
            # Creates a sample from a wave
            return audiocore.RawSample(self.wave, sample_rate=self.sample_rate)

        def _get_note_waves(self):
            # Here, we are looping over note's waves, in order to reach the product
            # of their wave's length.
            waves = []
            for note in self.notes:
                repeat_count = int(self.scsn / len(note.sine_wave))
                wave = note.sine_wave * repeat_count
                waves.append(wave)
            return waves

        def _get_sine_wave(self):
            # Creates the average wave
            sine_wave = array.array("h", [0] * self.scsn)
            for i, values in enumerate(zip(self.waves)):
                # Numbers here cannot exceed the max height (2 bytes),
                # so we average the results
                sine_wave[i] = sum(values) // len(values)
            return sine_wave

    FREQUENCIES = (440, 523.25, 659.26)
    NOTES = [Note(freq) for freq in FREQUENCIES]
    CHORD = Chord(NOTES)

    def main():
        dac = audiopwmio.PWMAudioOut(board.GP0)
        dac.play(CHORD.sample, loop=True)
        time.sleep(1)
        dac.stop()

    main()


I'm not happy with the result.
The sound is not nice but this is normal.
You can try it `here <https://www.mathsisfun.com/physics/audio-spectrum-beats.html>`_.
Set those values 440, 523.25, 659.26 and please, lower the volume before listening.
I got this same sound here, which looks good.
I'm pretty sure there's a trick, that developers was using in the past, that I'll have to find out.
That's all for today.



Next steps
----------

 * From a MIDI signal, play sounds.
 * Order more switches, and leard about `key matrices <https://www.pcbheaven.com/wikipages/How_Key_Matrices_Works/>`_
 * `GPIO expanders <https://www.adafruit.com/product/732>`_
 * `Build expanders PCBs <https://www.youtube.com/watch?v=lq6jbXaX4oQ&t=427>`_
 * Understand chomatic accordion keyboards (more on this later)


Resources
+++++++++

 * `Get started with micropython on Rapsberry pi pico <https://store.rpipress.cc/collections/latest-bookazines/products/get-started-with-micropython-on-raspberry-pi-pico>`_
 * `How a key matrix works <https://www.pcbheaven.com/wikipages/How_Key_Matrices_Works/>`_
 * `midi_usb <https://docs.circuitpython.org/en/latest/shared-bindings/usb_midi/index.html#>`_
 * All those posts from `notes and volts <https://www.notesandvolts.com>`_ are gold:

   * `Midi for the Arduino - Circuit Analysis <https://www.notesandvolts.com/2014/11/midi-and-arduino-circuit-analysis.html>`_
   * `Midi for the Arduino - Build a Midi Input Circuit <https://www.notesandvolts.com/2015/02/midi-and-arduino-build-midi-input.html>`_
   * `Midi for the Arduino - Arduino MIDI Library Input Test <https://www.notesandvolts.com/2015/02/midi-for-arduino-input-test.html>`_
   * And a lot more…
