My Ergodox EZ
=============

| A few months ago, I decided to buy an ergonomic keyboard.
| I wanted it to have the most used keys near my most agile fingers.
| I've selected 3 keyboards: The Truly Ergonomic, the Typematrix and the Ergodox.
| After a few time, I decided that I wanted to customise my layout in order to reduce movement and finally chosen the Ergodox EZ.
| 
| Before I even received it, I spent time to build a layout.
| I watched the builtin one an was pretty sure it will not fit my needs.
| I'm a python developper which use vim  and AwesomeWm.
| It means that ``super`` is one of the keys I use the most, that's why I put it right under my left, thumb, as well as the ``Space`` key.
| Index is the most agile finger, and this is why I put some over-user keys, like ``Enter``, ``Tabulation``, ``Backspace``,  and ``Delete`` at the center of the keyboard.
| I customised the ``qwerty`` layout and put some python special chars under the index as well, like ``,``, ``.`` and ``"``.
| Also, as there's not french chars on the qwerty layout, I use ``xcompose`` with ``Menu`` as my compose key. For example ``é`` is ``Menu, ' and e``, and so on. (see my `.XCompose <https://gitlab.com/mmequignon/nixos-config/-/blob/master/matthieu/dotfiles/xcompose>`_)
| After one year of daily use, here is the layout I'm using right now.

.. raw:: html

   <div style="padding-top: 60%; position: relative;">
       <iframe src="https://configure.zsa.io/embed/ergodox-ez/layouts/x9JJ3/latest/0" style="border: 0; height: 100%; left: 0; position: absolute; top: 0; width: 100%"></iframe>
   </div>

As you can see, it's still a work in progress, some keys aren't allocated, but it already suits me.

I would like to switch from qwerty to a more customized layout where the most used keys are placed where they are more easy to reach.

Oryx
++++

| Oryx is a layout editor developped by ZSA Technology Labs.
| It works great, and is easy to easy to use.
| You can configure not only the layout but also some keyboard properties, like the delay after a key is considered tapped or holded.
| You're able to adopt specific behaviours depending on your needs an preferences, which is what I was looking for.

.. figure:: ../_static/ergodox_settings.png
   :width: 80%


Ergodox EZ on NixOs
+++++++++++++++++++

| To make the keyboard work on linux, you just have to add few udev rules.
| On NixOs, just add this line in ``configuration.nix``.

.. code-block:: nix

   hardware.keyboard.zsa.enable = true;

You'll also need the client in order to push your layouts on the keyboard. On NixOs, the package name is ``wally-cli``.

Ergodox updates
+++++++++++++++

| Since I bought it, there been a few updates. For example, I can now get some html code to embed on my blog in order to share my layout and more importantly: macros had been added.
| The maximum number of keys you can add to a macro is 4, which I find a bit limited.
