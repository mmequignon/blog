with import <nixpkgs> {};
with python3Packages;

mkShell {
  propagatedBuildInputs = [
    sphinx
    sphinx_rtd_theme
  ];
}
